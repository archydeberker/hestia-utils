#!/bin/sh
docker build --progress=plain \
  -t hestia-utils-python:test \
  -f tests/Dockerfile \
  .
docker run --rm \
  --env-file .env \
  -v ${PWD}/coverage:/app/coverage \
  -v ${PWD}/hestia_earth:/app/hestia_earth \
  -v ${PWD}/tests:/app/tests \
  hestia-utils-python:test
