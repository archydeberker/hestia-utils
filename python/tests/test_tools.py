from datetime import datetime
import unittest
import numpy as np
from hestia_earth.schema import NodeType

from hestia_earth.utils.tools import non_empty_value, non_empty_list, is_term, current_time_ms
from hestia_earth.utils.tools import safe_parse_float, safe_parse_date


class TestTools(unittest.TestCase):
    def test_non_empty_value(self):
        self.assertEqual(non_empty_value(''), False)
        self.assertEqual(non_empty_value(1), True)
        self.assertEqual(non_empty_value([]), False)
        self.assertEqual(non_empty_value(False), True)

    def test_non_empty_list(self):
        self.assertEqual(non_empty_list(['', 1, [], False]), [1, False])

    def test_is_term(self):
        self.assertEqual(is_term({'@type': NodeType.CYCLE.value}), False)
        self.assertEqual(is_term({'type': NodeType.TERM.value}), True)

    def test_current_time_ms(self):
        self.assertTrue(current_time_ms() > 1000)

    def test_safe_parse_float(self):
        self.assertEqual(safe_parse_float('123.456'), 123.456)
        self.assertEqual(safe_parse_float('abcd', 10), 10)
        self.assertEqual(safe_parse_float(np.nan, 1), 1)

    def test_safe_parse_date(self):
        self.assertEqual(safe_parse_date('2020-01-01'), datetime(2020, 1, 1))
        self.assertEqual(safe_parse_date('abcd', datetime(2020, 1, 1)), datetime(2020, 1, 1))
