import unittest
import numpy

from .utils import fixtures_path
from hestia_earth.utils.lookup import load_lookup, column_name, get_table_value, download_lookup


class TestLookup(unittest.TestCase):
    def test_load_lookup_numpy_array(self):
        lookup = load_lookup(f"{fixtures_path}/lookup.csv")
        self.assertEqual(type(lookup), numpy.recarray)

    def test_get_table_value(self):
        lookup = load_lookup(f"{fixtures_path}/lookup.csv")

        # single column match
        self.assertEqual(get_table_value(lookup, column_name('Col1'), 'val10', column_name('Col3')), 'val30')
        # multiple column match
        self.assertEqual(get_table_value(lookup, [
            column_name('Col1'),
            column_name('Col2'),
        ], [
            'val10',
            'val21'
        ], column_name('Col3')), 'val31')
        # no match
        self.assertEqual(get_table_value(lookup, column_name('Col10'), 'val10', column_name('Col3')), None)

        # column does not exist
        self.assertEqual(get_table_value(lookup, [
            column_name('Col1'),
            column_name('Col2'),
        ], [
            'random',
            'val21'
        ], column_name('random')), None)

    def test_download_lookup_success(self):
        filename = 'crop.csv'
        lookup = download_lookup(filename)
        self.assertEqual(type(lookup), numpy.recarray)

    def test_download_lookup_error(self):
        filename = 'random file that does not exist.csv'
        lookup = download_lookup(filename)
        self.assertEqual(lookup, None)

    def test_handle_missing_values(self):
        # missing float value
        filename = 'measurement.csv'
        lookup = download_lookup(filename)
        self.assertEqual(get_table_value(lookup, 'termid', 'rainfallPeriod', 'maximum'), None)

        # missing string value
        filename = 'crop.csv'
        lookup = download_lookup(filename)
        self.assertEqual(get_table_value(lookup, 'termid', 'fixedNitrogen', 'combustion_factor_crop_residue'), None)
