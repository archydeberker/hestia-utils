import unittest

from hestia_earth.utils.request import join_args, request_url


class TestRequest(unittest.TestCase):
    def test_join_args(self):
        self.assertEqual(join_args(['arg1', '', 'arg3']), 'arg1&arg3')

    def test_request_url(self):
        self.assertEqual(request_url('base', id='id', empty=''), 'base?id=id')
