import unittest

from hestia_earth.utils.model import find_term_match, find_primary_product, convert_value


class TestModel(unittest.TestCase):
    def test_find_term_match(self):
        term_id = 'my term'
        term = {'@id': term_id}
        values = [{'term': term}, {'term': {'@id': 'another id'}}]

        self.assertEqual(find_term_match(values, term_id), {'term': term})

    def test_find_primary_product(self):
        primary = {
            '@type': 'Product',
            'primary': True
        }
        cycle = {
            'products': [
                primary,
                {'@type': 'Product'}
            ]
        }

        self.assertEqual(find_primary_product(cycle), primary)

    def test_convert_value(self):
        self.assertEqual(convert_value(1, 'm3', 'kg', density=553), 553)
        self.assertEqual(convert_value(1, 'm3', 'L'), 1000)
        self.assertEqual(convert_value(553, 'kg', 'm3', density=553), 1)
        self.assertEqual(convert_value(553, 'kg', 'L', density=553), 1000)
        self.assertEqual(convert_value(1000, 'L', 'm3'), 1)
        self.assertEqual(round(convert_value(100, 'MJ', 'kWh')), 28)
        self.assertEqual(convert_value(10, 'kWh', 'MJ'), 36)
