import unittest

from hestia_earth.utils.date import diff_in_years, is_in_days


class TestUtils(unittest.TestCase):
    def test_diff_in_years(self):
        self.assertEqual(diff_in_years('1990-01-01', '1999-02-01'), 9.1)

    def test_is_in_days(self):
        self.assertTrue(is_in_days('2000-01-01'))
        self.assertFalse(is_in_days('2000'))
