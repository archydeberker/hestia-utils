import unittest
from unittest.mock import patch
import requests
from hestia_earth.schema import SchemaType

from hestia_earth.utils.request import api_url
from hestia_earth.utils.api import find_related, download_hestia, node_exists, find_node, find_node_exact


fake_related_response = [{'@id': 'related_id'}]
fake_download_response = {'@id': 'id', '@type': 'type'}


class FakeFindRelatedSuccess():
    def json():
        return fake_related_response


class FakeFindRelatedError():
    def json():
        return {}


class FakeFindRelatedException():
    def json():
        raise requests.exceptions.RequestException('error')


class FakeDownloadSuccess():
    def json():
        return fake_download_response


class FakeDownloadError():
    def json():
        raise requests.exceptions.RequestException('error')


class FakeNodeExistSuccess():
    def json():
        return fake_download_response


class FakeNodeExistError():
    def json():
        return {"message": "not-found", "details": {}}


class TestApi(unittest.TestCase):
    @patch('requests.get', return_value=FakeFindRelatedSuccess)
    def test_find_related_success(self, mock_get):
        res = find_related(SchemaType.CYCLE, 'id', SchemaType.SOURCE)
        self.assertEqual(res, fake_related_response)
        mock_get.assert_called_once_with(
            f"{api_url()}/cycles/id/sources?limit=100", headers={'Content-Type': 'application/json'})

    @patch('requests.get', return_value=FakeFindRelatedError)
    def test_find_related_error(self, _m):
        res = find_related(SchemaType.CYCLE, 'id', SchemaType.SOURCE)
        self.assertEqual(res, None)

    @patch('requests.get', return_value=FakeFindRelatedException)
    def test_find_related_exception(self, _m):
        res = find_related(SchemaType.CYCLE, 'id', SchemaType.SOURCE)
        self.assertEqual(res, None)

    @patch('requests.get', return_value=FakeDownloadSuccess)
    def test_download_hestia_success(self, mock_get):
        res = download_hestia('id', SchemaType.SOURCE)
        self.assertEqual(res, fake_download_response)
        mock_get.assert_called_once_with(
            f"{api_url()}/sources/id", headers={'Content-Type': 'application/json'})

    @patch('requests.get', return_value=FakeDownloadError)
    def test_download_hestia_error(self, _m):
        res = download_hestia('id', SchemaType.SOURCE)
        self.assertEqual(res, None)

    @patch('requests.get', return_value=FakeNodeExistSuccess)
    def test_node_exists_true(self, _m):
        self.assertEqual(node_exists('id', SchemaType.SOURCE), True)

    @patch('requests.get', return_value=FakeNodeExistError)
    def test_node_exists_false(self, _m):
        self.assertEqual(node_exists('id', SchemaType.SOURCE), False)

    def test_find_node(self):
        name = 'Wheat'
        res = find_node(SchemaType.TERM, {'name': name}, 2)
        self.assertTrue(res[0].get('name').startswith(name))

    def test_find_node_exact(self):
        name = 'Wheat'
        res = find_node_exact(SchemaType.TERM, {'name': name})
        self.assertEqual(res, None)

        name = 'Wheat, grain'
        res = find_node_exact(SchemaType.TERM, {'name': name})
        self.assertEqual(res.get('name'), name)
