# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.4.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.4...v0.4.5) (2021-03-31)


### Features

* **python api:** handle passing access token for api requests ([2e7bb6a](https://gitlab.com/hestia-earth/hestia-utils/commit/2e7bb6a11a16e999eed3a893f2e03817928ae97b))

### [0.4.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.3...v0.4.4) (2021-03-26)


### Features

* **python api:** handle download node with `dataState` ([016a4ce](https://gitlab.com/hestia-earth/hestia-utils/commit/016a4ce3a36a12271608fd21d6c4830a881d6652))


### Bug Fixes

* **python api:** handle search for nested fields ([c5fe34a](https://gitlab.com/hestia-earth/hestia-utils/commit/c5fe34a33e627a8359072da8a14f20f46e5aeaf0))

### [0.4.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.2...v0.4.3) (2021-03-19)


### Bug Fixes

* **lookup:** handle missing int/float/string values ([afe23d4](https://gitlab.com/hestia-earth/hestia-utils/commit/afe23d48d3f6b679f3cb6de05d3651682c682637))

### [0.4.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.1...v0.4.2) (2021-03-19)


### Features

* **python lookup:** handle download without bucket env var ([9d48b0b](https://gitlab.com/hestia-earth/hestia-utils/commit/9d48b0b833a33fc032462fcc24297b943ab0e8eb))

### [0.4.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.4.0...v0.4.1) (2021-03-17)


### Features

* add function to convert a value from one unit to another ([c9f8b33](https://gitlab.com/hestia-earth/hestia-utils/commit/c9f8b33e6b18a90b88fe3aebd2ae7ceac8db78e9))

## [0.4.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.3.3...v0.4.0) (2021-03-15)


### ⚠ BREAKING CHANGES

* **csv:** sort option on `toCsv` has been removed.
Please use `headersFromCsv` to extract headers first, then `sortKeysByType`
from the schema library to sort the headers, and pass them to `toCsv` method

### Features

* **api:** add method `node_exists` ([9dd975a](https://gitlab.com/hestia-earth/hestia-utils/commit/9dd975a517559c4a04c4abf256a193d85e96f7d2))


### Bug Fixes

* **csv:** remove sorting of headers in favor of schema ([af61632](https://gitlab.com/hestia-earth/hestia-utils/commit/af61632449386f920b4f416ad03911280fea6517))

### [0.3.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.3.2...v0.3.3) (2021-03-09)


### Features

* **tools:** handle parse float `nan` ([78ea7cc](https://gitlab.com/hestia-earth/hestia-utils/commit/78ea7cc8f15696c714544c4a821dfa5517474224))


### Bug Fixes

* handle no value for `isIri` ([afb1195](https://gitlab.com/hestia-earth/hestia-utils/commit/afb11959ed66bb7d8363eb702fe9f9dae433ba46))

### [0.3.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.3.1...v0.3.2) (2021-02-27)


### Bug Fixes

* export date functions ([6c17ad8](https://gitlab.com/hestia-earth/hestia-utils/commit/6c17ad834f2ee6471aa5c8f34018aad0c5bf2bd3))

### [0.3.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.3.0...v0.3.1) (2021-02-27)


### Features

* **date:** add methods diff in days / years ([7d8d5f9](https://gitlab.com/hestia-earth/hestia-utils/commit/7d8d5f9d4d5cc19c1f9de6c7011d98b890ba20e4))
* **number:** add `toPrecision` function ([8a3bfc0](https://gitlab.com/hestia-earth/hestia-utils/commit/8a3bfc0619920af2e15c85f98d605169bcf8f0df))

## [0.3.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.2.1...v0.3.0) (2021-02-23)


### ⚠ BREAKING CHANGES

* **api:** `search_url` function has been removed

### Features

* **array:** add `intersection` function ([4512af7](https://gitlab.com/hestia-earth/hestia-utils/commit/4512af765f3172d1fe6a4dcf8d9840de952c31ad))
* **python:** update schema requirement to `2.0.0` ([3ae8b5f](https://gitlab.com/hestia-earth/hestia-utils/commit/3ae8b5f6ee09376c497a4f20efb3d932eba64e83))


* **api:** use api url for searching ([38f01a0](https://gitlab.com/hestia-earth/hestia-utils/commit/38f01a03a00e7e460845e31837e16712f6f6ea79))

### [0.2.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.2.0...v0.2.1) (2021-02-03)


### Bug Fixes

* **python api:** handle error message find related ([3e2475b](https://gitlab.com/hestia-earth/hestia-utils/commit/3e2475b7a0683331291607314f1e2f3efc8d5a39))

## [0.2.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.8...v0.2.0) (2021-02-01)


### Features

* **python:** add method to return time in ms ([3115d18](https://gitlab.com/hestia-earth/hestia-utils/commit/3115d1840addba36313328f22d02b5e6f3f3a7de))
* **python:** add safe parse float and date methods ([7c7fb55](https://gitlab.com/hestia-earth/hestia-utils/commit/7c7fb55a1c77af42f31abb3f018fb84a98f64001))
* **python lookup:** add option to store file in memory ([c10b259](https://gitlab.com/hestia-earth/hestia-utils/commit/c10b259a4e62616d84a47452f62f805084b39041))


### Bug Fixes

* **python utils:** fix default prod web url ([69ff382](https://gitlab.com/hestia-earth/hestia-utils/commit/69ff382ab3e157865f76e66bf4684032f5d559d4))

### [0.1.8](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.7...v0.1.8) (2021-01-22)


### Bug Fixes

* **python s3:** fix issue with too many requests in threads ([82477b2](https://gitlab.com/hestia-earth/hestia-utils/commit/82477b28b7efb2daadc0196edfafbb20d5edb22f))

### [0.1.7](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.6...v0.1.7) (2021-01-20)


### Features

* **javascript csv:** convert Date to JSON format ([d248836](https://gitlab.com/hestia-earth/hestia-utils/commit/d248836ab21d1d4088e6f3dd180312acb6ae8f8d))


### Bug Fixes

* **javascript csv:** handle GeoJSON as string or object ([3861494](https://gitlab.com/hestia-earth/hestia-utils/commit/38614941b8ae7a9310f73d550d0093b142e6e179))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.5...v0.1.6) (2021-01-16)


### Features

* **javascript csv:** ad `toCsv` method ([a86ce90](https://gitlab.com/hestia-earth/hestia-utils/commit/a86ce90c892c50490c77913e1ebc0e6f18131503))

### [0.1.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.4...v0.1.5) (2021-01-14)


### Bug Fixes

* **python lookup:** fix lookup decode content ([4f46f3d](https://gitlab.com/hestia-earth/hestia-utils/commit/4f46f3d3cc861ec0bd423685da5fda64a9eef2c1))

### [0.1.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.3...v0.1.4) (2021-01-14)


### Features

* **api:** use get URL instead of download for better performance ([3fd2c59](https://gitlab.com/hestia-earth/hestia-utils/commit/3fd2c594ef444fc3d967435260c967fdf7af4b83))


### Bug Fixes

* **s3 client:** return raw data when loading from bucket ([418ce2b](https://gitlab.com/hestia-earth/hestia-utils/commit/418ce2b0b1bc550d90f9b0045201c7527740d038))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.2...v0.1.3) (2021-01-12)


### Bug Fixes

* **python lookup:** fix lookup path on bucket ([d7f06e3](https://gitlab.com/hestia-earth/hestia-utils/commit/d7f06e35502ac34f61cfb39ab8da91e5e47217f3))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.1...v0.1.2) (2021-01-07)


### Features

* **javascript:** add utils ([d727c9a](https://gitlab.com/hestia-earth/hestia-utils/commit/d727c9aeea04eb8c45a47d3d46915a3272a1ffe2))


### Bug Fixes

* **api:** handle no results when searching for node ([4f05d47](https://gitlab.com/hestia-earth/hestia-utils/commit/4f05d47563751aa8daff6da488c9723a619b61e3))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.1.0...v0.1.1) (2020-12-23)


### Features

* **python:** add methods filter dict/list ([fffece9](https://gitlab.com/hestia-earth/hestia-utils/commit/fffece9bf04acb7a114439a9125979691067f6dc))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.7...v0.1.0) (2020-12-17)


### ⚠ BREAKING CHANGES

* **python api:** method linked to API are now in `hestia_earth.utils.api`

### Features

* **python:** add method to download lookup from glossary ([44f9301](https://gitlab.com/hestia-earth/hestia-utils/commit/44f9301bc5d26e354c1675b81af49838265dfc59))


### Bug Fixes

* **python api:** handle errors on download node not found ([bce14fd](https://gitlab.com/hestia-earth/hestia-utils/commit/bce14fd68dce14f23922a2c6ec7f2bd543dda4eb))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.6...v0.0.7) (2020-12-16)


### Bug Fixes

* **lookup:** handle `IndexError` for `get_table_value` ([cff8c0a](https://gitlab.com/hestia-earth/hestia-utils/commit/cff8c0afaea543b819db18b069ffa2bb6af9cade))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.5...v0.0.6) (2020-12-15)


### Features

* **python:** add lookup table methods ([897502f](https://gitlab.com/hestia-earth/hestia-utils/commit/897502f2c717cfbecb555ff17b64a2c01b17d69d))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.4...v0.0.5) (2020-12-10)


### Features

* **python:** improves speed of retrieving on s3 ([f8131ff](https://gitlab.com/hestia-earth/hestia-utils/commit/f8131ffb420d843bb46a53dca9fe3e6c0128b3ab))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.3...v0.0.4) (2020-12-10)


### Features

* **python:** handle key not found on bucket ([318e2fd](https://gitlab.com/hestia-earth/hestia-utils/commit/318e2fd2e1a5e96ed1a0e08362713600e1612f2b))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.2...v0.0.3) (2020-12-10)


### Features

* **python:** add method to get related nodes ([869a3c9](https://gitlab.com/hestia-earth/hestia-utils/commit/869a3c917370bb0950bc9945e44230baf628963d))


### Bug Fixes

* **python:** handle no term in `find_term_match` ([f547d74](https://gitlab.com/hestia-earth/hestia-utils/commit/f547d74eaa261b18655b5b542ad530f7703a481f))

### [0.0.2](https://gitlab.com/hestia-earth/hestia-utils/compare/v0.0.1...v0.0.2) (2020-12-08)


### Bug Fixes

* **utils:** fix incorrect os env access ([3643f98](https://gitlab.com/hestia-earth/hestia-utils/commit/3643f98e76e35ca72c09d1fd51d37a4fd5824d97))

### 0.0.1 (2020-12-08)


### Features

* **python:** add functions to download and search nodes ([ee4040c](https://gitlab.com/hestia-earth/hestia-utils/commit/ee4040ca16748a2048d88beac9920214ae5c9fb1))
