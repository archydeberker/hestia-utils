export const isBoolean = (value: string) => value.toLowerCase() === 'true' || value.toLowerCase() === 'false';
