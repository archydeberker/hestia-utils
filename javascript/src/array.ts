/**
 * Return list of unique values.
 *
 * @param values List of values
 */
export const unique = <T>(values: T[]): T[] => [...new Set(values)];

/**
 * Return an array containing all values that appear in both arrays.
 *
 * @param array1 List of values
 * @param array2 List of values
 */
export const intersection = <T>(array1: T[], array2: T[]) => array1.filter(x => array2.includes(x));
