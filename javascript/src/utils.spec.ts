import { expect } from 'chai';
import 'mocha';

import { filterUndefinedValues, isEmpty, isIri, isUndefined, reduceUndefinedValues } from './utils';

describe('utils', () => {
  describe('filterUndefinedValues', () => {
    it('should remove undefined values', () => {
      const values = [[], {}, 123, 'abc', null, undefined, [1], { a: 1 }];
      expect(filterUndefinedValues(values)).to.deep.equal([123, 'abc', [1], { a: 1 }]);
    });
  });

  describe('isEmpty', () => {
    it('should return true for empty value', () => {
      expect(isEmpty('')).to.equal(true);
      expect(isEmpty(null)).to.equal(true);
      expect(isEmpty(undefined)).to.equal(true);
      expect(isEmpty([])).to.equal(true);
      expect(isEmpty({})).to.equal(true);
      expect(isEmpty({ a: 1 }, 2)).to.equal(true);
    });

    it('should return false for non-empty value', () => {
      expect(isEmpty('a')).to.equal(false);
      expect(isEmpty(1)).to.equal(false);
      expect(isEmpty(true)).to.equal(false);
      expect(isEmpty([1])).to.equal(false);
      expect(isEmpty({ a: 1 })).to.equal(false);
    });
  });

  describe('isIri', () => {
    it('should return true if it starts with http', () => {
      expect(isIri('https://www.hestia.earth')).to.equal(true);
      expect(isIri('hestia.earth')).to.equal(false);
      expect(isIri()).to.equal(false);
    });
  });

  describe('isUndefined', () => {
    it('should return true for undefined value', () => {
      expect(isUndefined(null)).to.equal(true);
      expect(isUndefined(undefined)).to.equal(true);
      expect(isUndefined([])).to.equal(true);
      expect(isUndefined({})).to.equal(true);
    });

    it('should return false for non-undefined value', () => {
      expect(isUndefined('')).to.equal(false);
      expect(isUndefined(1)).to.equal(false);
      expect(isUndefined(true)).to.equal(false);
      expect(isUndefined([1])).to.equal(false);
      expect(isUndefined({ a: 1 })).to.equal(false);
    });
  });

  describe('reduceUndefinedValues', () => {
    it('should remove undefined values', () => {
      const values = {
        a: [],
        b: {},
        c: 123,
        d: 'abc',
        e: null,
        f: undefined,
        g: [1],
        h: { a: 1 },
        i: '',
        j: 0
      };
      expect(reduceUndefinedValues(values)).to.deep.equal({
        c: 123,
        d: 'abc',
        g: [1],
        h: { a: 1 },
        i: '',
        j: 0
      });
    });
  });
});
