import { expect } from 'chai';
import 'mocha';

import { isBoolean } from './boolean';

describe('boolean', () => {
  describe('isBoolean', () => {
    it('should return true for boolean only', () => {
      expect(isBoolean('abc')).to.equal(false);
      expect(isBoolean('abc123')).to.equal(false);
      expect(isBoolean('123')).to.equal(false);
      expect(isBoolean('false')).to.equal(true);
    });
  });
});
