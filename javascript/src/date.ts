export const secondMs = 1000;
export const minuteMs = 60 * secondMs;
export const hourMs = 60 * minuteMs;
export const dayMs = 24 * hourMs;
const year = 365.2425;

/**
 * Get the difference in days between two dates.
 *
 * @param date1 The left date
 * @param date2 The right date
 *
 * @returns The difference between date1 and date2 in days (rounded).
 */
export const diffInDays = (date1: Date | string, date2: Date | string) =>
  Math.abs(Math.round((new Date(date2).getTime() - new Date(date1).getTime()) / dayMs));

/**
 * Get the difference in years between two dates.
 *
 * @param date1 The left date
 * @param date2 The right date
 *
 * @returns The difference between date1 and date2 in years (precision: 1).
 */
export const diffInYears = (date1: Date | string, date2: Date | string) =>
  Math.round((diffInDays(date1, date2) / year) * 10) / 10;
