export const isNumber = (n: string | number) => !isNaN(parseFloat(`${n}`)) && isFinite(parseFloat(`${n}`));

/**
 * Returns a number with significant figures.
 * Example: 3645.875 with 3 significant figures will return 3650.
 *
 * @param n The value
 * @param precision The number of significant figures
 */
export const toPrecision = (n: number, precision = 3) => {
  const mult = Math.pow(10, precision - Math.floor(Math.log(n < 0 ? -n : n) / Math.LN10) - 1);
  return n === 0 ? 0 : Math.round(n * mult) / mult;
};

type convertUnits = 'm3' | 'kg' | 'L' | 'MJ' | 'kWh';

const converters = {
  m3: {
    kg: (value: number, density: number) => value * density,
    L: (value: number) => value * 1000
  },
  kg: {
    m3: (value: number, density: number) => value / density,
    L: (value: number, density: number) => value / density * 1000
  },
  L: {
    kg: (value: number, density: number) => value * density / 1000,
    m3: (value: number) => value / 1000
  },
  kWh: {
    MJ: (value: number) => value * 3.6
  },
  MJ: {
    kWh: (value: number) => value / 3.6
  }
};

/**
 * Converts a value of unit into a different unit.
 * Depending on the destination unit, additional arguments might be provided.
 *
 * @param n The value to convert, usually a float or an integer.
 * @param fromUnit The unit the value is specified in.
 * @param toUnit The unit the converted value should be.
 * @param args Optional arguments to provide depending on the conversion.
 * @returns The converted value.
 */
export const convertValue = (n: number, fromUnit: convertUnits, toUnit: convertUnits, ...args): number =>
  converters[fromUnit][toUnit](n, ...args);
