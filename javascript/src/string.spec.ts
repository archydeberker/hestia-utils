import { expect } from 'chai';
import 'mocha';

import { ellipsis, keyToLabel, now } from './string';

describe('string', () => {
  describe('ellipsis', () => {
    it('should truncate long text', () => {
      expect(ellipsis('some very long text', 10)).to.equal('some very ...');
    });

    it('should not truncate short text', () => {
      expect(ellipsis('some text')).to.equal('some text');
    });

    it('should ignore no text', () => {
      expect(ellipsis()).to.equal('');
    });
  });

  describe('keyToLabel', () => {
    it('shuld transform as label', () => {
      expect(keyToLabel('randomKey_with_chars')).to.equal('Random Key with chars');
    });
  });

  describe('now', () => {
    it('should create a string', () => {
      expect(now().length).to.equal(10);
    });
  });
});
