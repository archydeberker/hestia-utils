import { isUndefined } from './utils';

export const filterParams = (obj: any) => Object.keys(obj).reduce((prev, key) => ({
  ...prev,
  ...(isUndefined(obj[key]) || obj[key] === '' ? {} : { [key]: `${obj[key]}` })
}), {});
