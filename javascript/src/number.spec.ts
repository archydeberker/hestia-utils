import { expect } from 'chai';
import 'mocha';

import { isNumber, toPrecision, convertValue } from './number';

describe('number', () => {
  describe('isNumber', () => {
    it('should return true for numbers only', () => {
      expect(isNumber('abc')).to.equal(false);
      expect(isNumber('abc123')).to.equal(false);
      expect(isNumber('false')).to.equal(false);
      expect(isNumber(123)).to.equal(true);
      expect(isNumber('123')).to.equal(true);
      expect(isNumber('5e-04')).to.equal(true);
    });
  });

  describe('toPrecision', () => {
    it('should use significant figures', () => {
      expect(toPrecision(9.5)).to.equal(9.5);
      expect(toPrecision(9.501)).to.equal(9.5);
      expect(toPrecision(9.49)).to.equal(9.49);
      expect(toPrecision(9.499)).to.equal(9.5);
      expect(toPrecision(0.000152)).to.equal(0.000152);
      expect(toPrecision(3645.875, 3)).to.equal(3650);
    });
  });

  describe('convertValue', () => {
    it('should convert kg to m3', () => {
      expect(convertValue(1, 'm3', 'kg', 553)).to.equal(553);
      expect(convertValue(1, 'm3', 'L')).to.equal(1000);
      expect(convertValue(553, 'kg', 'm3', 553)).to.equal(1);
      expect(convertValue(553, 'kg', 'L', 553)).to.equal(1000);
      expect(convertValue(1000, 'L', 'm3')).to.equal(1);
      expect(Math.round(convertValue(100, 'MJ', 'kWh'))).to.equal(28);
      expect(convertValue(10, 'kWh', 'MJ')).to.equal(36);
    });
  });
});
