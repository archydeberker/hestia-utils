import { expect } from 'chai';
import 'mocha';

import { diffInDays, diffInYears } from './date';

describe('date', () => {
  describe('diffInDays', () => {
    it('should return the difference in days', () => {
      expect(diffInDays('04/02/2014', '11/04/2014')).to.equal(216);
      expect(diffInDays('12/02/2014', '11/04/2014')).to.equal(28);
    });
  });

  describe('diffInYears', () => {
    it('should return the difference in days', () => {
      expect(diffInYears('04/02/2014', '11/04/2018')).to.equal(4.6);
      expect(diffInYears('12/02/2018', '11/04/2016')).to.equal(2.1);
    });
  });
});
