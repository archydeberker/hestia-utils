export const ellipsis = (text = '', maxlength = 20) =>
  text.length > maxlength ? `${text.substring(0, maxlength)}...` : text;

export const keyToLabel = (key: string) =>
  `${key[0].toUpperCase()}${key.replace(/([a-z])([A-Z])/g, '$1 $2').substring(1)}`.replace(/[_-]/g, ' ');

export const now = () => new Date().toJSON().substring(0, 10);
