export const isEmpty = (value: any, minKeys = 1) =>
  value === null ||
  typeof value === 'undefined' ||
  (
    typeof value === 'object' ?
      (
        Array.isArray(value) ?
          !value.length :
          Object.keys(value).filter(key => key !== 'type').length < minKeys
      ) :
      value === ''
  );

export const isIri = (value?: string) => (value || '').startsWith('http');

export const isUndefined = <T>(value: T) =>
  value === null ||
  typeof value === 'undefined' || (
    typeof value === 'object' && !(value instanceof Date) && !Object.keys(value).length
  );

export const reduceUndefinedValues = <T>(obj: T) =>
  Object.keys(obj).reduce((prev, key) => {
    const value = obj[key];
    return { ...prev, ...(isUndefined(value) ? {} : { [key]: value }) };
  }, {} as Partial<T>);

export const filterUndefinedValues = <T>(values: T[]) => values.filter(value => !isUndefined(value));
