import { expect } from 'chai';
import 'mocha';

import { intersection, unique } from './array';

describe('array', () => {
  describe('intersection', () => {
    it('should select the intersection', () => {
      const array1 = [1, 2, 3, 4];
      const array2 = [2, 4, 5, 6];
      expect(intersection(array1, array2)).to.deep.equal([2, 4]);
    });
  });

  describe('unique', () => {
    it('should return unique values', () => {
      const values = [1, 2, 2, 4];
      expect(unique(values)).to.deep.equal([1, 2, 4]);
    });
  });
});
