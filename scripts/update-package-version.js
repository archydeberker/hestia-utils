const { readFileSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '../'));
const version = require(join(ROOT, 'package.json')).version;

// python
const PYTHON_VERSION_PATH = resolve(join(ROOT, 'python', 'hestia_earth', 'utils', 'version.py'));
let content = readFileSync(PYTHON_VERSION_PATH, 'UTF-8');
content = content.replace(/VERSION\s=\s\'[\d\-a-z\.]+\'/, `VERSION = '${version}'`);
writeFileSync(PYTHON_VERSION_PATH, content, 'UTF-8');

// JavaScript
const JS_VERSION_PATH = resolve(join(ROOT, 'javascript', 'package.json'));
content = require(JS_VERSION_PATH);
content.version = version;
writeFileSync(JS_VERSION_PATH, JSON.stringify(content, null, 2), 'UTF-8');

const JS_LOCK_VERSION_PATH = resolve(join(ROOT, 'javascript', 'package-lock.json'));
content = require(JS_LOCK_VERSION_PATH);
content.version = version;
writeFileSync(JS_LOCK_VERSION_PATH, JSON.stringify(content, null, 2), 'UTF-8');
